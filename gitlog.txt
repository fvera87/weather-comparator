commit a244b4c899008b1ed80b25cf7236cd0cc987e2d3
Author: fvera87 <fernando.vl87@gmail.com>
Date:   Mon Sep 3 11:54:28 2018 +0200

    little change in readme

commit c9731d442459c2887cce599c9534e028b29ad62d
Author: fvera87 <fernando.vl87@gmail.com>
Date:   Mon Sep 3 11:41:07 2018 +0200

    improve error handling and add readme.md

commit 77cc410a17097e71558409d3ce3ea136f638757e
Author: fvera87 <fernando.vl87@gmail.com>
Date:   Mon Sep 3 10:53:16 2018 +0200

    improve frontend app with basic functionality

commit 0c3123946e5729acfaedf86b8deeae53276929d5
Author: fvera87 <fernando.vl87@gmail.com>
Date:   Mon Sep 3 10:32:37 2018 +0200

    include trim for cities

commit c8ad80857e6bf6270209962b7521d47d44c7e4c9
Author: fvera87 <fernando.vl87@gmail.com>
Date:   Mon Sep 3 10:30:44 2018 +0200

    include bundle to deal with cross origin requests

commit 5a88a5c614208cf6d52af71d2a9ee2b9f7908200
Author: fvera87 <fernando.vl87@gmail.com>
Date:   Mon Sep 3 09:57:34 2018 +0200

    adjust heuristic for evaluate how good a weather is

commit b89940a944da8502fe322236a3a7e21e73ceed49
Author: fvera87 <fernando.vl87@gmail.com>
Date:   Mon Sep 3 09:33:22 2018 +0200

    Add files that were not added before, also previous commit has basic frontend app to consume the api

commit 49cc81c0f16fcbaae6f0414b7c136f4f54f84481
Author: fvera87 <fernando.vl87@gmail.com>
Date:   Mon Sep 3 09:32:11 2018 +0200

    correct a few things and add redis functionallity

commit d78b4a556604e31ef60e09aef11743a4e27eca6e
Author: fvera87 <fernando.vl87@gmail.com>
Date:   Mon Sep 3 08:56:29 2018 +0200

    Ignore: Add files that were not added before

commit a8a386b9c4cb164c3f06806ce0a68896c7c5a58d
Author: fvera87 <fernando.vl87@gmail.com>
Date:   Mon Sep 3 08:55:19 2018 +0200

    First step, new controller, service and basic functionality

commit bd4a75688b07f5dde8d8f2d3550d9ed26712ff83
Author: fvera87 <fernando.vl87@gmail.com>
Date:   Sun Sep 2 12:48:50 2018 +0200

    Initial commit
