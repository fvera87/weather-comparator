# Readme compare weather app for XING

#### Contents
The app is splitted in 2:
 - First, a Symfony app that offers an API (due to my lack of experience with Laravel I though it made more sense to me to write this test app with it)
 - Second, a vue.js app that offers a basic consumer of the app in the form of SAP
 
#### Symfony app

The app is a very basic app that offers an api that might be consumed by whoever might need it, that might be frontend apps, mobile apps, or other services within the xing organization.

To the basic skeleton of a normal Symfony app, I added an Entity to represent the Weather Data, a controller to route all our incoming request, and a service to perform the business logic.
I also added a Redis cache in order to avoid as many calls as possible, with a ttl of 10 minutes that I think it should be enough. 
I followed a very simple heuristic with the data that the api for the weather offers, starting from a certain number, certain conditions that we want to avoid to have good weather, cost points. This could have been done different obviously.
I decided to create an interface for this weather api, and a basic implementation for it of openweathermap, in order to be able to readapt to changes there easily.
I also left different things behind. Most importantly, I didn't write any test. Normally I would had liked to write some unit test for each class (mocking its dependencies) and some integration tests to see that everything works together. 
Some other options that I considered were another source of Data Storage, but I didn't see that necessary with Redis and keeping something more permanent is not useful for our logic.
 
#### Vue app

I decided to use Vue.js as it's the FE-framework I feel more confortable with. 

The app is very simple, there is a form, and the user has to introduce exactly 5 cities separated by commas. 
The api takes that imput, and returns the best cities, that are then displayed with minimum styling.

#### How to use it

Both apps run separately, so both has to be started to interact with each other.
The symfony app runs on port 8000, the vue app runs on port 8080. There is a redirection from the main path of symfony app to the vue app.
Then, it's just a simple form.