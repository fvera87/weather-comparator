<?php

namespace App\Util;

class Util {
    public static function cleanString($string) {
        return preg_replace('/[^A-Za-z0-9, ]/', '', $string); // Removes special chars.
    }
}
?>
