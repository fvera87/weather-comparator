<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class ApiController extends Controller
{
    const HTTP_OK = 200;
    const HTTP_CREATED = 201;
    const HTTP_BAD_REQUEST = 400;
    const HTTP_UNAUTHORIZED = 401;
    const HTTP_NOT_FOUND = 404;
    const HTTP_SERVER_ERROR = 500;
    const HTTP_SERVER_UNAVAILABLE = 503;
    /**
     * Here we could add more different http response codes... just a few as an example
     */


    /**
     * @var integer HTTP status code - 200 (OK) by default
     */
    protected $statusCode = self::HTTP_OK;

    /**
     * Gets the value of statusCode.
     *
     * @return integer
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Sets the value of statusCode.
     *
     * @param integer $statusCode the status code
     *
     * @return self
     */
    protected function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Returns a JSON response
     *
     * @param array $data
     * @param array $headers
     *
     * @return JsonResponse
     */
    public function respond($data, $headers = [])
    {
        return new JsonResponse($data, $this->getStatusCode(), $headers);
    }

    /**
     * Sets an error message and returns a JSON response
     *
     * @param string $errors
     *
     * @return JsonResponse
     */
    public function respondWithErrors($errors, $headers = [])
    {
        $data = [
            'errors' => $errors,
        ];

        return new JsonResponse($data, $this->getStatusCode(), $headers);
    }

    /**
     * Returns a 400 Bad request http response
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function respondBadRequest($message = 'Bad request!')
    {
        return $this->setStatusCode(self::HTTP_BAD_REQUEST)->respondWithErrors($message);
    }

    /**
     * Returns a 401 Unauthorized http response
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function respondUnauthorized($message = 'Not authorized!')
    {
        return $this->setStatusCode(self::HTTP_UNAUTHORIZED)->respondWithErrors($message);
    }

    /**
     * Returns a 404 Not Found
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function respondNotFound($message = 'Not found!')
    {
        return $this->setStatusCode(self::HTTP_NOT_FOUND)->respondWithErrors($message);
    }

    /**
     * Returns a 500 error message
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function respondServerError($message = 'There was a server error')
    {
        return $this->setStatusCode(self::HTTP_SERVER_ERROR)->respondWithErrors($message);
    }

    /**
     * Returns a 503 server unavailable method
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function respondServerUnavailable($message = 'There server is right now unavailable')
    {
        return $this->setStatusCode(self::HTTP_SERVER_ERROR)->respondWithErrors($message);
    }


}