<?php
namespace App\Controller;
use App\Service\WeatherService;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Routing\Annotation\Route;
use App\Util\Util;

class WeatherController extends ApiController {

    private $weatherService;
    public function __construct(WeatherService $wService)
    {
        $this->weatherService = $wService;
    }


    /**
     * @Route("/api/weather/{name}")
     */
    public function weatherAction($name)
    {
        $name = Util::cleanString($name);
        $weather = $this->weatherService->getWeatherFromCity($name);
        return $this->respond($weather);
    }
     /**
     * @Route("/api/weather/compare/{query}")
     */
    public function compareWeatherAction($query)
    {
        $query = Util::cleanString($query);
        $cities = explode(",", $query);
        if (count($cities) != 5) return $this->respondBadRequest("There should be exactly 5 valid city names to be compared as an input, please check it again!");
        $bestCities = [];
        $bestGrade = 0;
        foreach ($cities as $city ) {
            $city = trim($city);
            try {
                $grade = $this->weatherService->getGradeFromCity($city);
                if ($grade > $bestGrade) {
                    $bestGrade = $grade;
                    $bestCities = array($city);
                } else if ($grade == $bestGrade) {
                    $bestCities[] = $city;
                }
            }catch(Exception $e) {
                return $this->respondServerError($e->getMessage());
            }
        }
        return $this->respond(json_encode($bestCities));
    }

}
?>
