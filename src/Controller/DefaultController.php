<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction() {
        // We want to redirect everything that is not requesting something from our api to the frontend app
        return $this->redirect('http://localhost:8080');
    }

}