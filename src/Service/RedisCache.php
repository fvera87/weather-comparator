<?php

namespace App\Service;

use Predis\Client;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class ScheduleService
 * @package AppBundle\Services
 */
class RedisCache
{
    const TTL_MINUTE=60;
    const TTL_HOUR=3660;
    const TTL_DAY=86400;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->client = $client = new Client();
        $this->serializer= $serializer;
    }

    /**
     * @param $key
     * @return array
     * @internal param $object
     */
    public function get($key)
    {
        return $this->client->get($key);
    }

    /**
     * @param $key
     * @return array
     */
    public function invalidate($key)
    {
        $this->client->del(array($key));
    }

    /**
     * @param $key
     * @param $value
     * @param int $ttl
     * @return object
     */
    public function set($key, $value, $ttl = 0)
    {
        if($ttl >0) {
            $this->client->setex($key, $ttl, $value);
        }else{
            $this->client->set($key, $value);
        }
    }

}