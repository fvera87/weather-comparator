<?php
namespace App\Service;

use App\Entity\WeatherData;

class WeatherService
{
    const REDIS_KEY = 'weather:city:';
    protected $weatherProvider;
    protected $redisCache;

    function __construct(WeatherProvider $wProvider, RedisCache $cache)
    {
        $this->weatherProvider = $wProvider;
        $this->redisCache = $cache;
    }

    function getWeatherFromCity($cityName)
    {
        $weather = $this->redisCache->get(self::REDIS_KEY.$cityName);
        if ($weather == null) {
            $weatherCall = $this->weatherProvider->getWeatherFromCity($cityName);
            $weather = $weatherCall->getBody()->getContents();
            $this->redisCache->set(self::REDIS_KEY.$cityName, $weather, RedisCache::TTL_MINUTE * 10);
        }
        return $weather;
    }
    function getGradeFromCity($cityName)
    {
        $weather_json = json_decode($this->getWeatherFromCity($cityName));
        $weatherData = new WeatherData($cityName,
            $weather_json->main->temp,
            $weather_json->main->temp_min,
            $weather_json->main->temp_max,
            $weather_json->main->humidity,
            $weather_json->wind->speed,
            $weather_json->clouds->all);
        return $weatherData->howGoodIsIt();
    }

}