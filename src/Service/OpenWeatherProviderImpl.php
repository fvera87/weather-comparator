<?php

namespace App\Service;

use GuzzleHttp\Client;

class OpenWeatherProviderImpl implements WeatherProvider
{
    protected $httpClient;

    protected $apiUrl = 'api.openweathermap.org/data/2.5/weather';
    protected $apiCode = 'cc82577ee7c87deab64c718ee5df3bd9';

    function __construct() {
        $this->httpClient = new Client();
    }

    public function getWeatherFromCity($cityName)
    {
        return $this->httpClient->get($this->apiUrl.'?q='.$cityName.'&appid='.$this->apiCode.'&units=metric');
    }
}