<?php
namespace App\Service;

interface WeatherProvider {
  public function getWeatherFromCity($cityName);
}
?>