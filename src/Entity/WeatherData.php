<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WeatherDataRepository")
 */
class WeatherData
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="float")
     */
    private $temperature;

    /**
     * @ORM\Column(type="float")
     */
    private $min_temperature;

    /**
     * @ORM\Column(type="float")
     */
    private $max_temperature;

    /**
     * @ORM\Column(type="integer")
     */
    private $humidity;

    /**
     * @ORM\Column(type="integer")
     */
    private $wind;

    /**
     * @ORM\Column(type="integer")
     */
    private $clouds;
    /**
     * @ORM\Column(type="integer")
     */
    private $rain;

    /**
     * WeatherData constructor.
     * @param $city
     * @param $temperature
     * @param $min_temperature
     * @param $max_temperature
     * @param $humidity
     * @param $wind
     * @param $clouds
     */
    public function __construct($city, $temperature, $min_temperature, $max_temperature, $humidity = 0, $wind = 0, $clouds = 0, $rain = 0)
    {
        $this->city = $city;
        $this->temperature = $temperature;
        $this->min_temperature = $min_temperature;
        $this->max_temperature = $max_temperature;
        $this->humidity = $humidity;
        $this->wind = $wind;
        $this->clouds = $clouds;
        $this->rain = $rain;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getTemperature(): ?float
    {
        return $this->temperature;
    }

    public function setTemperature(float $temperature): self
    {
        $this->temperature = $temperature;

        return $this;
    }

    public function getMinTemperature(): ?float
    {
        return $this->min_temperature;
    }

    public function setMinTemperature(float $min_temperature): self
    {
        $this->min_temperature = $min_temperature;

        return $this;
    }

    public function getMaxTemperature(): ?float
    {
        return $this->max_temperature;
    }

    public function setMaxTemperature(float $max_temperature): self
    {
        $this->max_temperature = $max_temperature;

        return $this;
    }

    public function getHumidity(): ?int
    {
        return $this->humidity;
    }

    public function setHumidity(int $humidity): self
    {
        $this->humidity = $humidity;

        return $this;
    }

    public function getWind(): ?int
    {
        return $this->wind;
    }

    public function setWind(int $wind): self
    {
        $this->wind = $wind;

        return $this;
    }

    public function getclouds(): ?int
    {
        return $this->clouds;
    }

    public function setclouds(int $clouds): self
    {
        $this->clouds = $clouds;

        return $this;
    }
    public function getRain(): ?int
    {
        return $this->rain;
    }

    public function setRain(int $rain): self
    {
        $this->rain = $rain;

        return $this;
    }


    /**
     * Simple heuristic to determine how good a weather might be.
     */
    public function howGoodIsIt() {
        $grade = 1000;
        // Is it cold?
        if ($this->temperature < 20) $grade -= 5;
        if ($this->temperature < 10) $grade -= 10;
        if ($this->temperature < 0) $grade -= 20;
        // Is it too warm?
        if ($this->temperature > 30) $grade -= 5;
        if ($this->temperature > 35) $grade -= 10;
        if ($this->temperature > 40) $grade -= 20;
        // will it get colder?
        if ($this->min_temperature < 15) $grade -= 5;
        if ($this->min_temperature < 5) $grade -= 10;
        if ($this->min_temperature < 0) $grade -= 20;
        // will it get warmer?
        if ($this->max_temperature > 30) $grade -= 5;
        if ($this->max_temperature > 35) $grade -= 10;
        if ($this->max_temperature > 40) $grade -= 20;
        //is it humid??
        if ($this->max_temperature > 30) $grade -= 5;
        if ($this->max_temperature > 40) $grade -= 10;
        if ($this->max_temperature > 50) $grade -= 20;
        //is it claudy??
        if ($this->clouds > 15) $grade -= 5;
        if ($this->clouds > 30) $grade -= 10;
        if ($this->clouds > 50) $grade -= 20;
        //is it windy??
        if ($this->wind > 5) $grade -= 5;
        if ($this->wind > 10) $grade -= 10;
        if ($this->wind > 20) $grade -= 20;
        //is it raining??
        if ($this->rain > 0) $grade -= 5;
        if ($this->rain > 2) $grade -= 10;
        if ($this->rain > 4) $grade -= 20;

        return $grade;

    }
}
